package cache

import (
	"fmt"
	"time"
)

type NoCache struct {
}

func NewNoCache(defaultExpiration time.Duration) NoCache {
	return NoCache{}
}

func (c NoCache) Get(key string, ptrValue interface{}) error {
	return ErrCacheMiss
}

func (c NoCache) GetMulti(keys ...string) (Getter, error) {
	err := fmt.Errorf("Not Implement")
	return c, err
}

func (c NoCache) Set(key string, value interface{}, expires time.Duration) error {
	return nil
}

func (c NoCache) Add(key string, value interface{}, expires time.Duration) error {
	return ErrNotStored
}

func (c NoCache) Replace(key string, value interface{}, expires time.Duration) error {
	return ErrNotStored
}

func (c NoCache) Delete(key string) error {
	return ErrCacheMiss
}

func (c NoCache) Increment(key string, n uint64) (newValue uint64, err error) {
	return 0, ErrCacheMiss
}

func (c NoCache) Decrement(key string, n uint64) (newValue uint64, err error) {
	return 0, ErrCacheMiss
}

func (c NoCache) FlushAll() error {
	return nil
}

func (c NoCache) Flush(namespace string) error {
	return nil
}
