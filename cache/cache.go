package cache

import (
	"errors"
	"fmt"
	"time"
)

// Length of time to cache an item.
const (
	DefaultExpiry = time.Duration(0)
	NeverExpire   = time.Duration(-1)
)

var (
	minUnicodeRuneValue = uint8(0)
	namespaceRuneValue  = string(minUnicodeRuneValue)
	defaultNamespace    = "default"
	//cacheLog = revel.RevelLog.New("section", "cache")
)

// Getter is an interface for getting / decoding an element from a cache.
type Getter interface {
	Get(key string, ptrValue interface{}) error
}

type Cache interface {
	// The Cache implements a Getter.
	Getter

	// Set the given key/value in the cache, overwriting any existing value
	// associated with that key.  Keys may be at most 250 bytes in length.
	//
	// Returns:
	//   - nil on success
	//   - an implementation specific error otherwise
	Set(key string, value interface{}, expires time.Duration) error

	// Get the content associated multiple keys at once.  On success, the caller
	// may decode the values one at a time from the returned Getter.
	//
	// Returns:
	//   - the value getter, and a nil error if the operation completed.
	//   - an implementation specific error otherwise
	GetMulti(keys ...string) (Getter, error)

	// Delete the given key from the cache.
	//
	// Returns:
	//   - nil on a successful delete
	//   - ErrCacheMiss if the value was not in the cache
	//   - an implementation specific error otherwise
	Delete(key string) error

	// Add the given key/value to the cache ONLY IF the key does not already exist.
	//
	// Returns:
	//   - nil if the value was added to the cache
	//   - ErrNotStored if the key was already present in the cache
	//   - an implementation-specific error otherwise
	Add(key string, value interface{}, expires time.Duration) error

	// Set the given key/value in the cache ONLY IF the key already exists.
	//
	// Returns:
	//   - nil if the value was replaced
	//   - ErrNotStored if the key does not exist in the cache
	//   - an implementation specific error otherwise
	Replace(key string, value interface{}, expires time.Duration) error

	// Increment the value stored at the given key by the given amount.
	// The value silently wraps around upon exceeding the uint64 range.
	//
	// Returns the new counter value if the operation was successful, or:
	//   - ErrCacheMiss if the key was not found in the cache
	//   - an implementation specific error otherwise
	Increment(key string, n uint64) (newValue uint64, err error)

	// Decrement the value stored at the given key by the given amount.
	// The value is capped at 0 on underflow, with no error returned.
	//
	// Returns the new counter value if the operation was successful, or:
	//   - ErrCacheMiss if the key was not found in the cache
	//   - an implementation specific error otherwise
	Decrement(key string, n uint64) (newValue uint64, err error)

	// Expire all cache entries immediately.
	// This is not implemented for the memcached cache (intentionally).
	// Returns an implementation specific error if the operation failed.
	FlushAll() error
	// Expire cache only for specific namespace immediately.
	// This is not implemented for the memcached cache (intentionally).
	// Returns an implementation specific error if the operation failed.
	Flush(namespace string) error
}

var (
	Instance Cache

	ErrCacheMiss = errors.New("cache: key not found.")
	ErrNotStored = errors.New("cache: not stored.")
)

// The package implements the Cache interface (as sugar).

func Get(key string, ptrValue interface{}) error                  { return Instance.Get(key, ptrValue) }
func GetMulti(keys ...string) (Getter, error)                     { return Instance.GetMulti(keys...) }
func Delete(key string) error                                     { return Instance.Delete(key) }
func Increment(key string, n uint64) (newValue uint64, err error) { return Instance.Increment(key, n) }
func Decrement(key string, n uint64) (newValue uint64, err error) { return Instance.Decrement(key, n) }
func FlushAll() error                                             { return Instance.FlushAll() }
func Flush(namespace string) error                                { return Instance.Flush(namespace) }
func Set(key string, value interface{}, expires time.Duration) error {
	return Instance.Set(key, value, expires)
}
func Add(key string, value interface{}, expires time.Duration) error {
	return Instance.Add(key, value, expires)
}
func Replace(key string, value interface{}, expires time.Duration) error {
	return Instance.Replace(key, value, expires)
}
func CreateKey(namespace string, attributes ...interface{}) string {
	if namespace == "" {
		namespace = defaultNamespace
	}
	k := namespace + namespaceRuneValue
	for _, att := range attributes {
		switch v := att.(type) {
		default:
			fmt.Printf("CreateKey: unexpected type %T\n", v)
			k += fmt.Sprintf("%v", att) + string(minUnicodeRuneValue)
		case string:
			str, _ := att.(string)
			k += str + string(minUnicodeRuneValue)
		case []string:
			arr, _ := att.([]string)
			for _, str := range arr {
				k += str + string(minUnicodeRuneValue)
			}
		}
	}
	return k
}
func SplitKey(key string) (string, []string, error) {
	componentIndex := 1
	components := []string{}
	for i := 1; i < len(key); i++ {
		if key[i] == uint8(minUnicodeRuneValue) {
			components = append(components, key[componentIndex:i])
			componentIndex = i + 1
		}
	}
	return components[0], components[1:], nil
}
func SetNamespaceRune(value string) {
	namespaceRuneValue = value
}
func SetMinUnicodeRune(value string) {
	minUnicodeRuneValue = uint8(value[0])
}

func SetCache(c Cache) {
	Instance = c
}
