package cache

import (
	"testing"
	"time"
)

var newRedisCache = func(_ *testing.T, defaultExpiration time.Duration) Cache {
	return NewRedisCache("localhost:6379", "", defaultExpiration)
}

// Test typical cache interactions
func TestRedisCache_TypicalGetSet(t *testing.T) {
	typicalGetSet(t, newRedisCache)
}

// Test the increment-decrement cases
func TestRedisCache_IncrDecr(t *testing.T) {
	incrDecr(t, newRedisCache)
}

func TestRedisCache_Expiration(t *testing.T) {
	expiration(t, newRedisCache)
}

func TestRedisCache_EmptyCache(t *testing.T) {
	emptyCache(t, newRedisCache)
}

func TestRedisCache_Replace(t *testing.T) {
	testReplace(t, newRedisCache)
}

func TestRedisCache_Add(t *testing.T) {
	testAdd(t, newRedisCache)
}

func TestRedisCache_GetMulti(t *testing.T) {
	testGetMulti(t, newRedisCache)
}

func TestRedisFlushAll(t *testing.T) {
	testFlushAll(t, newRedisCache)
}

//func TestRedisFlush(t *testing.T) {
//	testFlush(t, newRedisCache)
//}
