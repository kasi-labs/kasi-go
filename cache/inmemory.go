package cache

import (
	"fmt"
	gocache "github.com/robfig/go-cache"
	"reflect"
	"time"
)

type InMemoryCache struct {
	defaultExpiration time.Duration
	cache             map[string]gocache.Cache
}

func NewInMemoryCache(defaultExpiration time.Duration) *InMemoryCache {
	return &InMemoryCache{
		defaultExpiration,
		make(map[string]gocache.Cache, 0),
	}
}

func (c *InMemoryCache) GetCache(key string) gocache.Cache {
	namespace, _, err := SplitKey(key)
	if err != nil {
		namespace = ""
	}
	if obj, found := c.cache[namespace]; found {
		return obj
	}
	obj := *gocache.New(c.defaultExpiration, time.Minute)
	c.cache[namespace] = obj
	return obj
}

func (c *InMemoryCache) Get(key string, ptrValue interface{}) error {
	cacheObj := c.GetCache(key)
	value, found := cacheObj.Get(key)
	if !found {
		if IsDebug() {
			fmt.Printf("Get cache key: %s miss!!\n", key)
		}
		return ErrCacheMiss
	}

	v := reflect.ValueOf(ptrValue)
	if v.Type().Kind() == reflect.Ptr && v.Elem().CanSet() {
		v.Elem().Set(reflect.ValueOf(value))
		if IsDebug() {
			fmt.Printf("Get cache key: %s\n", key)
		}
		return nil
	}

	err := fmt.Errorf("cache: attempt to get %s, but can not set value %v", key, v)
	return err
}

func (c *InMemoryCache) GetMulti(keys ...string) (Getter, error) {
	err := fmt.Errorf("Not Implement")
	return c, err
}

func (c *InMemoryCache) Set(key string, value interface{}, expires time.Duration) error {
	// NOTE: go-cache understands the values of DEFAULT and FOREVER
	cacheObj := c.GetCache(key)
	cacheObj.Set(key, value, expires)
	if IsDebug() {
		fmt.Printf("Set cache key: %s\n", key)
	}
	return nil
}

func (c *InMemoryCache) Add(key string, value interface{}, expires time.Duration) error {
	cacheObj := c.GetCache(key)
	err := cacheObj.Add(key, value, expires)
	if err == gocache.ErrKeyExists {
		return ErrNotStored
	}
	return err
}

func (c *InMemoryCache) Replace(key string, value interface{}, expires time.Duration) error {
	cacheObj := c.GetCache(key)
	if err := cacheObj.Replace(key, value, expires); err != nil {
		return ErrNotStored
	}
	return nil
}

func (c *InMemoryCache) Delete(key string) error {
	cacheObj := c.GetCache(key)
	if found := cacheObj.Delete(key); !found {
		return ErrCacheMiss
	}
	return nil
}

func (c *InMemoryCache) Increment(key string, n uint64) (newValue uint64, err error) {
	cacheObj := c.GetCache(key)
	newValue, err = cacheObj.Increment(key, n)
	if err == gocache.ErrCacheMiss {
		return 0, ErrCacheMiss
	}
	return
}

func (c *InMemoryCache) Decrement(key string, n uint64) (newValue uint64, err error) {
	cacheObj := c.GetCache(key)
	newValue, err = cacheObj.Decrement(key, n)
	if err == gocache.ErrCacheMiss {
		return 0, ErrCacheMiss
	}
	return
}

func (c *InMemoryCache) FlushAll() error {
	for _, cacheObj := range c.cache {
		cacheObj.Flush()
	}
	return nil
}

func (c *InMemoryCache) Flush(namespace string) error {
	if cacheObj, found := c.cache[namespace]; found {
		cacheObj.Flush()
		return nil
	}
	return nil
}
