package cache

import (
	"time"
)

var (
	isInitialized = false
	debug         = false
)

var defaultExpiration = 24 * time.Hour // The default for the default is one day.
func init() {
	Instance = NewInMemoryCache(defaultExpiration)
	isInitialized = true
}

func IsDebug() bool {
	return debug
}
