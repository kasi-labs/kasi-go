module gitlab.com/kasi-labs/kasi-go

go 1.12

require (
	github.com/ThreeDotsLabs/watermill v1.0.0
	github.com/ThreeDotsLabs/watermill-googlecloud v1.0.2
	github.com/ThreeDotsLabs/watermill-http v1.0.2
	github.com/garyburd/redigo v1.6.0
	github.com/google/go-cmp v0.3.1
	github.com/gorilla/websocket v1.4.1
	github.com/ibm-messaging/mq-golang v0.0.0-20190820103725-19b946c185a8
	github.com/robfig/go-cache v0.0.0-20130306151617-9fc39e0dbf62
	google.golang.org/api v0.6.0
)
