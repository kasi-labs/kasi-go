package ibmmq

import "gitlab.com/kasi-labs/kasi-go/message"

type Queue struct {
	popper *Popper
	pusher *Pusher
}

func NewQueue(config Config) (*Queue, error) {
	popper, err := NewPopper(config)
	if err != nil {
		return &Queue{}, err
	}
	pusher, err := NewPusher(config)
	if err != nil {
		return &Queue{}, err
	}
	return &Queue{
		popper: popper,
		pusher: pusher,
	}, nil
}

func (q *Queue) Push(messages ...*message.Message) error {
	return q.pusher.Push(messages...)
}

func (q *Queue) Pop() (*message.Message, error) {
	return q.popper.Pop()
}
