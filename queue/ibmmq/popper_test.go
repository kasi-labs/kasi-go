package ibmmq

import (
	"fmt"
	"testing"
)

func BenchmarkPusherPopper_PushPop(b *testing.B) {
	config := NewConfig("./env.json")
	fmt.Printf("config %+v", config)
	popper, err := NewPopper(config)
	if err != nil {
		b.Error(err)
	}

	for i := 0; i < b.N; i++ {
		_, err := popper.Pop()
		if err != nil {
			b.Error(err)
		}
	}

	popper.Close()
}
