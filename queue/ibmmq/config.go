package ibmmq

import (
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go/queue/ibmmq/mqutils"
)

const (
	Provider = "ibmmq"
)

type Config struct {
	Connection mqutils.Env

	Marshaler Marshaler

	gmo *ibmmq.MQGMO // MQ Get MessageOptions
	pmo *ibmmq.MQPMO // MQ Put MessageOptions
}

func NewConfig(configFile string) Config {

	return Config{
		Connection: mqutils.LoadFromFile(configFile),
		Marshaler:  DefaultMarshaler{},

		gmo: DefaultGMO(),
		pmo: DefaultPMO(),
	}
}

// Provide default MQ Get MessageOptions
func DefaultGMO() *ibmmq.MQGMO {
	gmo := ibmmq.NewMQGMO()

	// The default options are OK, but it's always
	// a good idea to be explicit about transactional boundaries as
	// not all platforms behave the same way.
	gmo.Options = ibmmq.MQGMO_NO_SYNCPOINT

	// Set options to wait for a maximum of 3 seconds for any new message to arrive
	gmo.Options |= ibmmq.MQGMO_WAIT
	gmo.WaitInterval = 3 * 1000 // The WaitInterval is in milliseconds

	return gmo
}

// Provide default MQ Put MessageOptions
func DefaultPMO() *ibmmq.MQPMO {
	pmo := ibmmq.NewMQPMO()
	pmo.Options = ibmmq.MQPMO_NO_SYNCPOINT
	return pmo
}
