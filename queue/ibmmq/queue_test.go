package ibmmq

import (
	"gitlab.com/kasi-labs/kasi-go/queue/unit_test"
	"testing"
)

// Test typical Queue push/pop
func TestIbmq_PushPop(t *testing.T) {
	config := NewConfig("../env.json")
	unit_test.TestTropicalPushPop(t, NewIbmmqFactory(config))
}
