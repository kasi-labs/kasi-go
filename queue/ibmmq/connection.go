package ibmmq

import (
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"gitlab.com/kasi-labs/kasi-go/queue/ibmmq/mqutils"
	"io/ioutil"
	"log"
	"sync"
)

var logger = log.New(ioutil.Discard, "Env: ", log.LstdFlags)

type connectionWrapper struct {
	config   Config
	msgStyle string
	qMgr     *ibmmq.MQQueueManager
	qObject  *ibmmq.MQObject

	pushWg sync.WaitGroup
	popWg  sync.WaitGroup
}

func newConnection(config Config, msgStyle string) (*connectionWrapper, error) {
	conn := &connectionWrapper{
		config:   config,
		msgStyle: msgStyle,
	}

	return conn, nil
}

func (q *connectionWrapper) IsConnected() bool {
	return q.qMgr != nil
}

// Establishes the connection to the MQ Server. Returns the
// Queue Manager if successful
func (c *connectionWrapper) Connect() error {
	qMgr, err := mqutils.CreateConnection(c.config.Connection)
	if err != nil {
		logError(err)
		return err
	}
	logger.Println("Connected to IBM MQ")
	c.qMgr = &qMgr

	return nil
}

func (q *connectionWrapper) IsOpened() bool {
	return q.qObject != nil
}

func (q *connectionWrapper) Open() error {
	qObject, err := mqutils.OpenQueue(*q.qMgr, q.msgStyle)
	if err != nil {
		logError(err)
		return err
	}
	q.qObject = &qObject
	return nil
}

func (q *connectionWrapper) autoConnect() error {
	if !q.IsConnected() {
		err := q.Connect()
		if err != nil {
			return err
		}
	}
	if !q.IsOpened() {
		err := q.Open()
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *connectionWrapper) Close() error {
	if c.IsOpened() {
		if err := c.qObject.Close(0); err != nil {
			c.qObject = nil
			c.qMgr = nil
			return err
		}
	}
	return nil
}

func (c *connectionWrapper) AutoCloseIfConnectionBroken(err error) {
	if mqReturn, ok := err.(*ibmmq.MQReturn); ok {
		if mqReturn.MQCC == ibmmq.MQCC_FAILED && mqReturn.MQRC == ibmmq.MQRC_CONNECTION_BROKEN {
			c.Close()
		}
	}
}

func logError(err error) {
	logger.Println(err)
	logger.Printf("Error Code %v", err.(*ibmmq.MQReturn).MQCC)
}
