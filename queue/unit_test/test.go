package unit_test

import (
	"gitlab.com/kasi-labs/kasi-go/message"
	"gitlab.com/kasi-labs/kasi-go/queue"
	"testing"
)

// Test against a generic Queue interface
// They should pass for all implementations.

// TestTropicalPushPop to test put data into queue and pop data out
func TestTropicalPushPop(t *testing.T, factory *queue.NewFactory) {
	var err error
	popper, err := factory.NewPopper()
	if err != nil {
		t.Error(err)
		return
	}
	pusher, err := factory.NewPusher()
	if err != nil {
		t.Error(err)
		return
	}

	value := "foo"
	msg := message.NewMessage("", []byte(value))

	if err = pusher.Push(msg); err != nil {
		t.Errorf("Error pushing a value: %s", err)
	}

	value = ""
	msg, err = popper.Pop()
	if err != nil {
		t.Errorf("Error getting a value: %s", err)
	}
	value = string(msg.Payload)

	if value != "foo" {
		t.Errorf("Expected to get foo back, got '%s'", value)
	}
}
