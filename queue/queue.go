package queue

import (
	"errors"
	"gitlab.com/kasi-labs/kasi-go/message"
)

type Pusher interface {
	Push(messages ...*message.Message) error
	Close() error
}

type Popper interface {
	Pop() (*message.Message, error)
	Close() error
}

type Connector interface {
	Connect() error
}

type QueueFactory interface {
	NewPopper() func() (Popper, error)
	NewPusher() func() (Pusher, error)
}

type NewFactory struct {
	NewPopper func() (Popper, error)
	NewPusher func() (Pusher, error)
}

var (
	PusherInstance Pusher
	PopperInstance Popper

	ErrQueueIsEmpty = errors.New("queue: is empty")
)

func Push(messages ...*message.Message) error { return PusherInstance.Push(messages...) }
func Pop() (*message.Message, error)          { return PopperInstance.Pop() }
func Close() error {
	if PusherInstance != nil {
		if err := PusherInstance.Close(); err != nil {
			return err
		}
	}
	if PopperInstance != nil {
		if err := PopperInstance.Close(); err != nil {
			return err
		}
	}
	return nil
}
