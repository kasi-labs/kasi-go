package httpPubsub

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill"
	watermillHttp "github.com/ThreeDotsLabs/watermill-http/pkg/http"
	watermillMessage "github.com/ThreeDotsLabs/watermill/message"
	"github.com/pkg/errors"
	"gitlab.com/kasi-labs/kasi-go/message"
	"net/http"
)

type Publisher struct {
	*connectionWrapper
	publisher watermillMessage.Publisher
}

func NewPublisher(httpConfig HttpConfig) (*Publisher, error) {
	config := NewConfig(httpConfig)
	conn, err := newConnection(config)
	if err != nil {
		return nil, err
	}

	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: httpConfig.InsecureSkipVerify}, // ignore expired SSL certificates
	}

	client := &http.Client{Transport: transCfg}

	marshalWithHttpHeaderMessageFunc := func(url string, msg *watermillMessage.Message) (*http.Request, error) {
		req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(msg.Payload))
		if err != nil {
			return nil, err
		}

		req.Header.Set(watermillHttp.HeaderUUID, msg.UUID)

		metadataJson, err := json.Marshal(msg.Metadata)
		if err != nil {
			return nil, errors.Wrap(err, "could not marshal metadata to JSON")
		}
		req.Header.Set(watermillHttp.HeaderMetadata, string(metadataJson))

		for key, value := range httpConfig.Headers {
			req.Header.Set(key, value)
		}
		return req, nil
	}

	logger := watermill.NewStdLogger(false, false)
	pub, err := watermillHttp.NewPublisher(watermillHttp.PublisherConfig{
		MarshalMessageFunc: marshalWithHttpHeaderMessageFunc,
		Client:             client,
	}, logger)
	if err != nil {
		panic(err)
	}

	return &Publisher{conn, pub}, nil
}

func (p *Publisher) Publish(topic string, messages ...*message.Message) error {
	for _, msg := range messages {
		url := p.config.Http.Url + "/" + topic
		msg := watermillMessage.NewMessage(msg.UUID, watermillMessage.Payload(msg.Payload))
		if err := p.publisher.Publish(url, msg); err != nil {
			return err
		}
	}
	return nil
}

func (p *Publisher) Close() error {
	return p.publisher.Close()
}
