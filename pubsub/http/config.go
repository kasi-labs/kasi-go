package httpPubsub

const (
	Provider = "http"
)

type Config struct {
	Marshaler Marshaler

	Http HttpConfig
}

type Header map[string]string

type HttpConfig struct {
	Url                string `yaml:"url"`
	InsecureSkipVerify bool   `yaml:"insecure-skip-verify"`
	Headers            Header `yaml:"headers"`
}

func NewConfig(httpConfig HttpConfig) Config {
	return Config{
		Marshaler: DefaultMarshaler{},
		Http:      httpConfig,
	}
}
