package httpPubsub

import (
	"gitlab.com/kasi-labs/kasi-go/message"
)

type Marshaler interface {
	Marshal(msg *message.Message) ([]byte, error)
}

type DefaultMarshaler struct {
}

func (d DefaultMarshaler) Marshal(msg *message.Message) ([]byte, error) {
	return msg.Payload, nil
}
