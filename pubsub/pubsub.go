package pubsub

import (
	"gitlab.com/kasi-labs/kasi-go/message"
)

type Factory interface {
	NewPublisher() func() (Publisher, error)
	NewSubscriber() func() (Subscriber, error)
}

type NewFactory struct {
	NewPublisher  func() (Publisher, error)
	NewSubscriber func() (Subscriber, error)
}

type Publisher interface {
	Publish(topic string, messages ...*message.Message) error
	Close() error
}

type Subscriber interface {
	Subscribe() (chan *message.Message, error)
	Close() error
}
