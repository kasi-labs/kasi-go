package websocket

const (
	Provider = "websocket"
)

type Config struct {
	Marshaler Marshaler
}

func NewConfig() Config {
	return Config{
		Marshaler: DefaultMarshaler{},
	}
}
