package websocket

import "github.com/gorilla/websocket"

type connectionWrapper struct {
	config     Config
	wsupgrader *websocket.Upgrader
}

func newConnection(wsupgrader *websocket.Upgrader) (*connectionWrapper, error) {
	conn := &connectionWrapper{
		config:     Config{},
		wsupgrader: wsupgrader,
	}

	return conn, nil
}
