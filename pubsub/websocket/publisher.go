package websocket

import (
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/kasi-labs/kasi-go/message"
	"net/http"
)

type Publisher struct {
	*connectionWrapper

	sentMsg chan *message.Message
	closing chan bool
}

func NewPublisher() (*Publisher, error) {
	var wsupgrader = &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	conn, err := newConnection(wsupgrader)
	if err != nil {
		return nil, err
	}

	var sentMsgCh = make(chan *message.Message, 10000)
	var closingCh = make(chan bool, 1)
	return &Publisher{
		conn,
		sentMsgCh,
		closingCh,
	}, nil
}

func (p *Publisher) Publish(topic string, messages ...*message.Message) error {
	for _, msg := range messages {
		p.sentMsg <- msg
	}
	return nil
}

func (p *Publisher) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	fmt.Printf("Got new connections\n")
	conn, err := p.wsupgrader.Upgrade(rw, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}
	defer func() {
		fmt.Printf("Close connections\n")
		conn.Close()
	}()
	for {
		select {
		case msg := <-p.sentMsg:
			err = conn.WriteMessage(websocket.TextMessage, msg.Payload)
			if err != nil {
				fmt.Println("write:", err)
				return
			}
		case <-p.closing:
			fmt.Printf("End publish message")
			break
		}
	}
}

func (p *Publisher) Close() error {
	p.closing <- true
	return nil
}
