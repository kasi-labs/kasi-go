package googlecloud

import (
	"github.com/ThreeDotsLabs/watermill"
	watermillGoogleCloud "github.com/ThreeDotsLabs/watermill-googlecloud/pkg/googlecloud"
	watermillMessage "github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/kasi-labs/kasi-go/message"
	"google.golang.org/api/option"
)

type Publisher struct {
	*connectionWrapper
	publisher watermillMessage.Publisher
}

func NewPublisher(googlecloud GoogleCloudConfig) (*Publisher, error) {
	config := NewConfig(googlecloud)
	conn, err := newConnection(config)
	if err != nil {
		return nil, err
	}

	clientOptions := []option.ClientOption{
		option.WithCredentialsFile(conn.config.GoogleCloud.CredentialsFile),
	}

	logger := watermill.NewStdLogger(false, false)
	pub, err := watermillGoogleCloud.NewPublisher(watermillGoogleCloud.PublisherConfig{
		ProjectID:     conn.config.GoogleCloud.ProjectId,
		ClientOptions: clientOptions,
	}, logger)
	if err != nil {
		panic(err)
	}

	return &Publisher{conn, pub}, nil
}

func (p *Publisher) Publish(topic string, messages ...*message.Message) error {
	for _, msg := range messages {
		msg := watermillMessage.NewMessage(msg.UUID, watermillMessage.Payload(msg.Payload))
		if err := p.publisher.Publish(topic, msg); err != nil {
			return err
		}
	}
	return nil
}

func (p *Publisher) Close() error {
	return p.publisher.Close()
}
