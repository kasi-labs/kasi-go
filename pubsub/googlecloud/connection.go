package googlecloud

type connectionWrapper struct {
	config Config
}

func newConnection(config Config) (*connectionWrapper, error) {
	conn := &connectionWrapper{
		config: config,
	}

	return conn, nil
}
