package googlecloud

const (
	Provider = "googlecloud"
)

type Config struct {
	Marshaler Marshaler

	GoogleCloud GoogleCloudConfig `json:"googlecloud",yaml:"googlecloud"`
}

type GoogleCloudConfig struct {
	Topic           string `yaml:"topic"`
	TopicPrefix     string `yaml:"topic-prefix"`
	ProjectId       string `yaml:"project-id"`
	CredentialsFile string `yaml:"credentials-file"`
}

func NewConfig(googlecloud GoogleCloudConfig) Config {
	return Config{
		Marshaler:   DefaultMarshaler{},
		GoogleCloud: googlecloud,
	}
}
