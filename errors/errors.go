// Package errors provide a simple a error wrapper to record a stack trace in each call.
package errors

import (
	"fmt"
	"runtime"
	"strings"
)

type wrapper struct {
	error
	stackTrace []Trace
}

// Trace provide information of the error that has been wrap.
type Trace struct {
	Message  string `json:"message"`
	FuncName string `json:"function"`
	Location string `json:"location"`
}

func NewTrace(pc uintptr, a ...interface{}) Trace {
	t := Trace{}
	f := runtime.FuncForPC(pc)
	path, lineNumber := f.FileLine(pc)
	paths := strings.Split(path, "/")
	t.Location = fmt.Sprintf("%v:%v", paths[len(paths)-1], lineNumber)
	t.FuncName = f.Name()
	if len(a) > 0 {
		t.Message = fmt.Sprint(a...)
	}
	return t
}

// Wrap create a error wrapper which contain information about where it is called.
func Wrap(err error, args ...interface{}) error {
	if err == nil {
		return nil
	}
	pc, _, _, _ := runtime.Caller(1)
	trace := NewTrace(pc, args...)

	switch err := err.(type) {
	case wrapper:
		err.stackTrace = append(err.stackTrace, trace)
		return err
	default:
		return wrapper{
			err,
			[]Trace{trace},
		}
	}
}

// GetStack return a stack trace from an error if there is one.
func GetStack(err error) []Trace {
	if err, ok := err.(wrapper); ok {
		return err.stackTrace
	}
	return nil
}
