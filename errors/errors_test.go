package errors

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestWrap(t *testing.T) {
	wrapLine := "53"
	var testcase = []struct {
		input      error
		inMsg      string
		errMsg     string
		stackTrace []Trace
	}{
		{
			input:  fmt.Errorf("test error message"),
			inMsg:  "Wrap1",
			errMsg: "test error message",
			stackTrace: []Trace{
				{
					Message:  "Wrap1",
					FuncName: "origincert-chaincode/errors.TestWrap",
					Location: "errors_test.go:" + wrapLine,
				},
			},
		},
		{
			input: nil,
			inMsg: "",
		},
		{
			input:  Wrap(fmt.Errorf("test error message"), "Wrap2"),
			inMsg:  "Wrap1",
			errMsg: "test error message",
			stackTrace: []Trace{
				{
					Message:  "Wrap2",
					FuncName: "origincert-chaincode/errors.TestWrap",
					Location: "errors_test.go:35",
				},
				{
					Message:  "Wrap1",
					FuncName: "origincert-chaincode/errors.TestWrap",
					Location: "errors_test.go:" + wrapLine,
				},
			},
		},
	}
	for i, c := range testcase {
		err := Wrap(c.input, c.inMsg)
		if (c.input == nil) != (err == nil) {
			t.Errorf(`case %02v expect error "%v" got "%v"`, i, c.errMsg, err)
			continue
		}
		if err == nil {
			continue
		}
		if err.Error() != c.errMsg {
			t.Errorf(`case %02v expect error "%v" got "%v"`, i, c.errMsg, err)
			continue
		}
		errWrapper, ok := err.(wrapper)
		if !ok {
			t.Errorf(`case %02v error is not a wrapper type`, i)
			continue
		}
		if c.stackTrace == nil {
			if errWrapper.stackTrace != nil {
				t.Errorf(`case %02v expect stack trace "%v" got "%v"`, i, c.stackTrace, errWrapper.stackTrace)
				continue
			}
		} else {
			if errWrapper.stackTrace == nil {
				t.Errorf(`case %02v expect stack trace "%v" got "%v"`, i, c.stackTrace, errWrapper.stackTrace)
				continue
			}
		}
		if len(c.stackTrace) != len(errWrapper.stackTrace) {
			t.Errorf(`case %02v expect stack trace with length "%v" got "%v"`, i, len(c.stackTrace), len(errWrapper.stackTrace))
			continue
		}
		for j := range c.stackTrace {
			if !cmp.Equal(c.stackTrace[j], errWrapper.stackTrace[j]) {
				t.Errorf("case %02v stack %02v \n\texpect %v\n\t   got %v", i, j, c.stackTrace[j], errWrapper.stackTrace[j])
				continue
			}
		}
	}
}

func TestGetStack(t *testing.T) {
	var testcase = []struct {
		input      error
		stackTrace []Trace
	}{
		{
			input:      fmt.Errorf("test error message"),
			stackTrace: nil,
		},
		{
			input:      nil,
			stackTrace: nil,
		},
		{
			input: Wrap(fmt.Errorf("test error message"), "Wrap"),
			stackTrace: []Trace{
				{
					Message:  "Wrap",
					FuncName: "origincert-chaincode/errors.TestGetStack",
					Location: "errors_test.go:108",
				},
			},
		},
	}
	for i, c := range testcase {
		res := GetStack(c.input)
		if len(c.stackTrace) != len(res) {
			t.Errorf(`case %02v expect stack trace with length "%v" got "%v"`, i, len(c.stackTrace), len(res))
			continue
		}
		for j := range c.stackTrace {
			if !cmp.Equal(c.stackTrace[j], res[j]) {
				t.Errorf("case %02v stack %02v \n\texpect %v\n\t   got %v", i, j, c.stackTrace[j], res[j])
				continue
			}
		}
	}
}
